const bcrypt = require('bcrypt');
const validator = require('validator');

const userRepo = require('../repons/mongo-repo');

class userService {
  constructor() {
    this.userRepo = userRepo;
  }
  async findById(userId) {
    const user = await this.userRepo.finduserbyId(userId);
    if (user) {
      return user;
    }
    throw new Error('user not found');
  }
  async createUser(userForm) {
    const error = {};
    const usernameRegex = /^[a-zA-Z0-9-.]+$/;
    if (!userForm.username || !usernameRegex.test(userForm.username)) {
      error['username'] = 'invalid username';
    }
    const checkUsername = await userRepo.findByIdUserName(userForm.username);
    if (checkUsername !== null) {
      if (userForm.username === checkUsername.username) {
        error['username'] = 'User Exits';
      }
    }

    if (!userForm.email || !validator.isEmail(userForm.email)) {
      error['email'] = 'invalid email';
    }
    const checkEmail = await userRepo.findByIdUserName(userForm.email);
    if (checkEmail !== null) {
      if (isEmail(userForm.email) === checkEmail.email) {
        error['email'] = 'Email Exits';
      }
    }

    if (!userForm.password) {
      error['password'] = 'invalid password';
    }

    if (userForm.password !== userForm.confirmPassword) {
      error['confirmPassword'] = 'confirmPassword mismatch';
    }


    if (Object.keys(error).length !== 0) {

      console.log(error);
      throw error;
    }


    // hash password
    const hashedPassword = await bcrypt.hash(userForm.password, 10);
    delete userForm.password;
    delete userForm.confirmPassword;
    userForm.hashedPassword = hashedPassword;
    // store to database
    const newUser = await this.userRepo.createuser(userForm);
    delete newUser.hashedPassword;
    return newUser;
  }
}
const userServices = new userService();

module.exports = userServices;