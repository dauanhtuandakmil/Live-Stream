const bcrypt = require('bcrypt');
const userRepo = require('../repons/mongo-repo');

class AuthService {
  constructor() {
    this.userRepo = userRepo;
  }
  async login(username, password) {
    const user = await this.userRepo.findByIdUserName(username);
    if (!user) {
      throw new Error('invalid username');
    }
    console.log(password);
    const isMatch = await bcrypt.compare(password, user.hashedPassword);
    if (isMatch) {
      delete user.hashedPassword;
      return user;
    }
    
    throw new Error('invalid password');
  }
}
const authservice = new AuthService();
module.exports = authservice;
