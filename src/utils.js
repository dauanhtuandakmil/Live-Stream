const path = require('path');
const fs = require('fs');

const srcDir = __dirname;
const root = path.dirname(srcDir);

const readFile = (path) => {
  return new Promise((resolve, reject) => {
    fs.readFile(path, (err, data) => { 
      return err ? reject(err) : resolve(data);
    });
  });
};

module.exports = {
  readFile,
  rootPath: root,
};
