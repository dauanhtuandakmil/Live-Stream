const { MongoClient } = require('mongodb');

class MongoDbDatabase {
  constructor() {
    this.db = null;
    this.dbUrl = 'mongodb://127.0.0.1:27017';
    this.databaseName = 'LiveStream';
  }
  async connect() {
    const client = await MongoClient.connect(this.dbUrl, {
      useNewUrlParser: true
    });
    this.db = client.db(this.databaseName);
    return this.db;
  }
  getDb() {
    return this.db;
  }
}
const mongodb = new MongoDbDatabase();
module.exports = mongodb;
