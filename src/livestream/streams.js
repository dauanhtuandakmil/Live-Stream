/**
 * Stream object
 */
class Stream {
  constructor(id, name) {
    this.name = name;
    this.id = id;
  }
}

const streamFactory = () => {
  /**
   * available streams
   * the id value is considered unique (provided by socket.io)
   */
  const streamList = [];

  return {
    addStream : (id, name) => {
      const stream = new Stream(id, name);
      streamList.push(stream);
    },

    removeStream : (id) => {
      let index = 0;
      while (index < streamList.length && streamList[index].id !== id) {
        index += 1;
      }
      streamList.splice(index, 1);
    },

    // update function
    update : (id, name) => {
      const stream = streamList.find(element => element.id === id);
      stream.name = name;
    },

    getStreams : () => {
      return streamList;
    },

    getById: (id) => {
      return streamList.find(stream => stream.id === id);
    }
  };
};

const streamManager = streamFactory();
module.exports = streamManager;
