module.exports = (io, streams) => {
  io.on('connection', (client) => {
    console.log('-- ' + client.id + ' joined --');
    client.emit('id', client.id);

    client.on('message', (details) => {
      const otherClient = io.sockets.connected[details.to];

      if (!otherClient) {
        return;
      }
      delete details.to;
      details.from = client.id;
      console.log('emit to', details.id);
      otherClient.emit('message', details);
    });

    client.on('readyToStream', (options) => {
      console.log('-- ' + client.id + ' is ready to stream --');

      streams.addStream(client.id, options.name);
    });

    client.on('update', (options) => {
      streams.update(client.id, options.name);
    });

    function leave() {
      console.log('-- ' + client.id + ' left --');
      streams.removeStream(client.id);
    }

    client.on('disconnect', leave);
    client.on('leave', leave);
  });
};
