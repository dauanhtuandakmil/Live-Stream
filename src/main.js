const express = require('express');
const path = require('path');
const ejs = require('ejs');
const socketio = require('socket.io');
const parse = require('body-parser');
const http = require('http');
const passport = require('passport');
const session = require('express-session');
const util = require('../src/utils');


const mongodb = require('../src/config/database');

const authRouter = require('../src/api/auth.api');
const streamRouter = require('./api/stream.api');
const streamManager = require('./livestream/streams');
const streamSocket = require('./livestream/socketHandler');


require('./passport');

const main = async () => {
  await mongodb.connect();

  const app = express();

  app.use(parse.urlencoded({
    extended: false,
  }));
  app.use(parse.json());
  // socket
  const server = http.createServer(app);
  const io = socketio(server);

  streamSocket(io, streamManager);

  io.on('connection', (socket) => {
    console.log('a user connected');
    socket.on('disconnect', () => {
      console.log('user disconnect');
    });
    socket.on('Client-send-Server', (data) =>{
      console.log(data);
      io.sockets.emit('Server-send-Client', { nd: data });
    });  
  });
  app.set('view engine', 'html');
  app.engine('html', ejs.renderFile);

  app.use(session({
    secret: 'thisisstring',
    resave: false,
    saveUninitialized: true,

    cookie: {
      maxAge: (7 * 24 * 60 * 60 * 1000)
    },
  }));
  app.use(passport.initialize());
  app.use(passport.session());
  // mặc định thì ejs sẽ tìm trong thư mục view
  app.set('View option', {
    layout: false
  });

  app.set('views', path.join(util.rootPath, 'views'));
  app.use('/public', express.static(path.join(util.rootPath, 'public')));
  app.use('/', authRouter);
  app.use('/api/streams', streamRouter);
  app.get('/', (req, res) => {
    res.render('index', {
      user: req.id,
    });
  });

  // GET streams as JSON
  const displayStreams = (req, res) => {
    const streamId = req.params.id;
    const stream = streamManager.getById(streamId);
    if (stream) {
      res.render('stream', { stream });
    } else {
      res.status(404).send('not found');
    }

  };
  const displayVideo = (req, res) => {
    res.render('home');
  };

  app.get('/streams/:id', displayStreams);
  app.get('/home', displayVideo);


  const port = +process.env.PORT || 1111;
  server.listen(port, '0.0.0.0', (err) => {
    if (err) {
      console.log('lỗi');
    }
    console.log(`listenin to port ${port}`);
  });
};
main();
