const passport = require('passport');
const { Strategy } = require('passport-local');

const authService = require('../src/service/auth.service')
const userService = require('../src/service/users.service');

passport.use(new Strategy(async (username, password, done) => {
  try {
    const user = await authService.login(username, password);
    console.log(user);
    done(null, user);
  } catch (error) {
    console.log(error);
    done(error);
  }
}));
// khi chứng thực thành công
passport.serializeUser((user, callback) => callback(null, user._id));// lựa chọn 1 mục đại diện cho người dùng để ghi ra cookie

passport.deserializeUser(async (id, callback) => {//  
  try {
    const user = await userService.findById(id);
    console.log(user);
    callback(null, user);
  } catch (error) {
    callback(error);
  }
});

