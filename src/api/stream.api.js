const { Router } = require('express');
const streamManager = require('../livestream/streams');

const allStreamsController = (req, resp) => {
  resp.json(streamManager.getStreams());
};

const streamRouteFactory = () => {
  const route = Router();
  route.get('/', allStreamsController);
  return route;
};

module.exports = streamRouteFactory();
