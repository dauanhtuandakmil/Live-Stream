const bcrypt = require('bcrypt');
const passport = require('passport');
const session = require('express-session');
const local = require('passport-local');
const {
  Router
} = require('express');
const userService = require('../service/users.service');
const userRepo = require('../repons/mongo-repo');
require('../passport');
const loginPage = async (req, res) => {
  res.render('login');
};
const Register = async (req, res) => {
  res.render('register');
};
const loi = async (req, res) => {
  res.render('loi');
};
const forgot = async (req, res) => {
  res.render('forgot');
};
const createUserController = async (req, res) => {
  try {
    const user = {
      username: req.body.fullname,
      email: req.body.email,
      password: req.body.phonenumber,
      confirmPassword: req.body.password,
    };
    const userNew = await userService.createUser(user);
    console.log(user);
    res.render('register', {
      userValue: user,
    });
  } catch (err) {
    console.log(err);
    res.json({
      err: err
    });
  }
};
const loginUserController = async (req, res) => {
  try {
    const user = {
      username: req.body.text,
      password: req.body.password,
    };
    console.log(user);
    const checkExits = await userRepo.findByIdUserName(user.username);
    if (checkExits !== null) {
      const comparePass = await bcrypt.compare(user.password, checkExits.hashedPassword);
      if (comparePass) {
        console.log('Success');
        res.render('index');
      } else {
        console.log('Fail');
        res.render('login');
      }
    } else {
      console.log('Do not have an account');
      res.render('login');
    }
  } catch (err) {
    res.json({
      err: err
    });
  }
};
const loginController = (req, res) => {
  res.redirect('/index');
};
const AccessController = (req, res) => {
  if (req.isAuthenticated()) {
    res.render('index');
  } else {
    res.redirect('/login');
  }
};
const LogoutController = (req, res) => {
  req.logout();
  res.redirect('/login');
}
const socket = (req, res) => {
  res.render('live');
}
const createRoters = () => {
  const authRouters = Router();
  authRouters.get('/login', loginPage);
  authRouters.get('/index', AccessController);
  authRouters.post('/login', passport.authenticate('local', {
    failureRedirect: '/login'
  }), loginController);
  authRouters.get('/register', Register);
  authRouters.post('/register', createUserController);
  authRouters.get('/loi', loi);
  authRouters.get('/forgot', forgot);
  authRouters.get('/logout', LogoutController);
  authRouters.get('/socket', socket);
  return authRouters;
};
const authRouter = createRoters();
module.exports = authRouter;