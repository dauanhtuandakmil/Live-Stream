const path = require('path');
const {
  ObjectId
} = require('mongodb');

const mongodb = require('../config/database');
class userMongoRepo {
  constructor() {
    this.mongodb = mongodb;
  }
  async createuser(userForm) {
    console.log('before insert');

    const insertUser = await this.userConllection().insertOne(userForm);
    console.log(userForm);
    return insertUser;
  }
  async findByIdUserName(username) {
    const user = await this.userConllection().findOne({
      username,
    });
    console.log(user);
    return user;
  }
  async finduserbyId(userId) {
    if (!ObjectId.isValid(userId)) {
      throw new TypeError('invalid user');
    }
    const user = await this.userConllection().findOne({
      _id: ObjectId(userId),
    });
    return user;
  }
  userConllection() {
    return this.mongodb.getDb().collection('users');
  }
}
const usermongoRepo = new userMongoRepo();
module.exports = usermongoRepo;