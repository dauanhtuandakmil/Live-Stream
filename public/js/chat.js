const socket = io('http://localhost:1111');

socket.on('Server-send-Client', (data) => {
  $('#list-message').append(`<div class='ms'> ${data.nd} </div>`);
});
$(document).ready(() => {
  $('#btnSendMessage').click(() => {
    socket.emit('Client-send-Server', $('#txtMessage').val());
  });
  $('#btnSendMessage').keyup((event) => {
    if (event.which === 13) {
      socket.emit('Client-send-Server', $('#txtMessage').val());
     }
  });
});
