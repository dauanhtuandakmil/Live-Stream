const client = new PeerManager();
const mediaConfig = {
  audio: false,
  video: {
    mandatory: {},
    optional: [],
  },
};

function cameraFactory() {
  const camera = {};
  camera.preview = document.getElementById('localStream');

  camera.start = () => requestUserMedia(mediaConfig)
    .then((stream) => {
      attachMediaStream(camera.preview, stream);
      client.setLocalStream(stream);
      camera.stream = stream;
    })
    .catch(Error('Failed to get access to local media.'));

  camera.stop = () => new Promise((resolve, reject) => {
    try {
      for (const track of camera.stream.getTracks()) {
        track.stop();
      }
      camera.preview.src = '';
      resolve();
    } catch (error) {
      reject(error);
    }
  });
  return camera;
}

function LocalStreamController(camera) {
  const localStream = {};
  localStream.name = 'Guest';
  localStream.link = '';
  localStream.cameraIsOn = false;

  localStream.toggleCam = () => {
    if (localStream.cameraIsOn) {
      camera.stop()
        .then(() => {
          client.send('leave');
          client.setLocalStream(null);
        })
        .catch(error => console.log(error));
    } else {
      camera.start()
        .then(() => {
          localStream.link = `${window.location.host}/${client.getId()}`;
          client.send('readyToStream', {
            name: localStream.name,
          });
        })
        .catch(err => console.log(err));
    }
  };
  return localStream;
}

const camera = cameraFactory();
camera.start();
const localCamManager = LocalStreamController(camera);
$("#start").click(() => {
  localCamManager.toggleCam();
});
$("#stop").click(() => {
  camera.stop();
});
